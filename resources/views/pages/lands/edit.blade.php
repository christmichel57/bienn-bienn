@extends("layouts.main")
@section("content")

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Quick Example</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{ route('lands.update',["id" => $land->id]) }}" method = "POST">

      @csrf
      @method('POST')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">libelle</label>
          <input type="text" name="libelle" class="form-control" id="exampleInputEmail1" placeholder="Entrez un libelle" value="{{ $land['libelle'] }}">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">description</label>
          <input type="text" name="description" class="form-control" id="exampleInputPassword1" placeholder="Entrez la description" value="{{ $land['description'] }}">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">code_indicatif</label>
          <input type="text" name="code_indicatif" class="form-control" id="exampleInputPassword1" placeholder="entrez le code indicatif" value="{{ $land['code_indicatif'] }}">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Continent</label>
          <select name="Continent" id="" >
            <option value="Afrique">Afrique</option>
            <option value="Asie">Asie</option>
            <option value="Oceanie">Oceanie</option>
            <option value="Europe">Europe</option>
            <option value="Amerique">Amerique</option>
        </select>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Population</label>
          <input type="text" name="population" class="form-control" id="exampleInputPassword1" placeholder="Entrez la population" value="{{ $land['population'] }}">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Capitale</label>
          <input type="text" name="capitale" class="form-control" id="exampleInputPassword1" placeholder="Entrez la capitale" value="{{ $land['capitale'] }}">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Monnaie</label>
          <select name="monnaie" id="">
              <option value="XOF">XOF</option>
              <option value="XOF">DOL</option>
              <option value="XOF">EUR</option>
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">langue</label>
          <select name="langue" id="">
              <option value="FR">FR</option>
              <option value="EN">EN</option>
              <option value="AN">AN</option>
              <option value="ES">ES</option>
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Superficie</label>
          <input type="text" name="superficie" class="form-control" id="exampleInputPassword1" placeholder="Entrez la superficie" value="{{ $land['superficie'] }}">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Est_laique?</label>
            <select name="est_laique" id="">
                <option value="1">Oui</option>
                <option value="0">Non</option>
            </select>
          </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Modifier</button>
      </div>
    </form>
  </div>

@endsection
