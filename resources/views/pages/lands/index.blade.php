@extends('layouts.main')

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tableau des Pays <a href="{{route('lands.create')}}"> <button class="btn btn-warning btn-circle btn-lg" type="button">+</button> </a></h3>

                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                        <div class="input-group-append">
                            <button type="submit" class="btn btn-default">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Libelle</th>
                            <th>Code indicatif</th>
                            <th>Continent</th>
                            <th>Population</th>
                            <th>Capitale</th>
                            <th>Monnaie</th>
                            <th>Langue</th>
                            <th>Superficie</th>
                            <th>Est laique</th>
                            <th>description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lands as $land)

                        <tr>
                            <td>{{$land['id']}}</td>
                            <td>{{$land['libelle']}}</td>
                            <td>{{$land['code_indicatif']}}</td>
                            <td>{{$land['Continent']}}</td>
                            <td>{{$land['population']}}</td>
                            <td>{{$land['capitale']}}</td>
                            <td>{{$land['monnaie']}}</td>
                            <td>{{$land['langue']}}</td>
                            <td>{{$land['superficie']}}</td>
                            <td>{{$land['est_laique']}}</td>
                            <td>{{$land['description']}}</td>
                            <td>
                                <a href="{{ route("lands.show", ["id" => $land->id]) }}"><button class="btn btn-info btn-circle" type="button"><i class="fa fa-eye"></i></button></a>
                                <a href="{{ route("lands.edit", ["id" => $land->id]) }}"><button class="btn btn-primary btn-circle" type="button"><i class="fa fa-pen"></i></button></a>
                                <a href=" {{ route("lands.destroy", ["id" => $land->id]) }} "><button class="btn btn-danger btn-circle" type="button"><i class="fa fa-trash"></i></button></a>
                            </td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
@endsection
