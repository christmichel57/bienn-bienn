<?php

use App\Http\Controllers\LandController;
use App\Models\Land;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/host');
});

Route::get('/lands', [LandController::class, 'index'])->name('lands.index');
Route::get('/lands/create', [LandController::class, 'create'])->name('lands.create');
Route::get('/lands/{id}', [LandController::class, 'show'])->name('lands.show');
Route::post('/lands', [LandController::class, 'store'])->name('lands.store');
Route::get('/lands/{id}/edit', [LandController::class, 'edit'])->name('lands.edit');
Route::post('/lands/{id}/update', [LandController::class, 'update'])->name('lands.update');
Route::get('/lands/{id}/destroy', [LandController::class, 'destroy'])->name('lands.destroy');
