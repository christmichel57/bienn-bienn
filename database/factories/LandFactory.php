<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

            $continents = array("Afrique" , "Europe" , "Amérique" , "Asie" , "Oceanie");
            $monnaie = array ("XOF","DOL","EUR");
            $langue = array ("FR","EN","AR","ES");

        return [

            'libelle' => $this->faker->country(),
            'description' => $this->faker->text(),
            'code_indicatif' => $this->faker->unique()->numerify("+###"),
            'Continent' => $continents[array_rand($continents)],
            'population' => rand(50000 , 1000000000). "habitants",
            'capitale' => $this->faker->city(),
            'monnaie' => $monnaie[array_rand($monnaie)],
            'langue' => $langue[array_rand($langue)],
            'superficie' => rand(2001 , 55511225) . "KM²",
            'est_laique' => $this->faker->boolean(),
        ];
    }
}
