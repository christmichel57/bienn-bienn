<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->id();

            $table->string("libelle");

            $table->text("description") -> nullable;

            $table->integer("code_indicatif");

            $table->string("Continent");

            $table->string("population");

            $table->string("capitale");

            $table->string("monnaie");

            $table->string("langue");

            $table->string("superficie");

            $table->boolean("est_laique");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
}
